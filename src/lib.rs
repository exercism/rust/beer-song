/// Emit verse by number.
pub fn verse(number: u32) -> String {
    const LIMIT: u32 = 100;
    let object: String = String::from("bottle");
    let objects: String = String::from("bottles");
    let content: String = String::from("beer");
    let support: String = String::from("on the wall");
    let no_quantity: String = String::from("No more");
    let action_default: String = String::from("Take one down and pass it around");
    let action_final: String = String::from("Go to the store and buy some more");
    let quantity: String;
    let less: String;

    let phrase: String = match number {
        0 => {
            quantity = no_quantity;
            less = (LIMIT - 1).to_string();
            format!(
                "{} {} of {} {}, {} {} of {}.\n{}, {} {} of {} {}.\n",
                quantity,
                objects,
                content,
                support,
                quantity.to_lowercase(),
                objects,
                content,
                action_final,
                less,
                objects,
                content,
                support
            )
        }
        1 => {
            quantity = number.to_string();
            less = no_quantity;
            format!(
                "{} {} of {} {}, {} {} of {}.\n{}, {} {} of {} {}.\n",
                quantity,
                object,
                content,
                support,
                quantity,
                object,
                content,
                action_default.replace("one", "it"),
                less.to_lowercase(),
                objects,
                content,
                support
            )
        }
        2 => {
            quantity = number.to_string();
            less = (number - 1).to_string();
            format!(
                "{} {} of {} {}, {} {} of {}.\n{}, {} {} of {} {}.\n",
                quantity,
                objects,
                content,
                support,
                quantity,
                objects,
                content,
                action_default,
                less,
                object,
                content,
                support
            )
        }
        _ => {
            quantity = number.to_string();
            less = (number - 1).to_string();
            format!(
                "{} {} of {} {}, {} {} of {}.\n{}, {} {} of {} {}.\n",
                quantity,
                objects,
                content,
                support,
                quantity,
                objects,
                content,
                action_default,
                less,
                objects,
                content,
                support
            )
        }
    };

    phrase
}

/// Sing verses from start to end inclusive.
pub fn sing(start: u32, end: u32) -> String {
    let mut song: String = String::new();

    for number in (end..=start).rev() {
        song.push_str(&verse(number));
        if number != end {
            song.push_str("\n");
        }
    }

    song
}
